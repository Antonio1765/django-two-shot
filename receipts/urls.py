from django.urls import path
from receipts.views import receipt_list

urlpatterns = [
    path("receipts/", receipt_list, name="receipt_list")
]
